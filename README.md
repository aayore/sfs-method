The SFS Method
==============

Continual improvement in knowledge-sharing.

The SFS Method project is where we design the SFS Method class.

The SFS Method class is meant to give subject-matter experts (SME's) a simple framework in which to share what they know.

Not as dangerous as this Method: https://whitelines.com/features/method-king-grabs.html