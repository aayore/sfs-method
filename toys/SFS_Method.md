SFS Method
==========

Introductions
-------------

Have students pair up. If there is an odd number of students, one student can 'partner' with the instructor, or one 'pair' can have three students. Have students count off, keeping pairs together.

0. The teacher should choose a student to introduce him or her. The student will say something like, "This is my friend David Willson. He is here to share whatever he has learned, that is teachable, about teaching." Then, the teacher will pantomime speaking from a podium, or illustrating a point, or some other gesture typical of a speaker or teacher.

1. All odd-numbered students introduce themselves to their even-numbered partners. They should tell their partner their name and purpose for attendance. This can happen simultaneously. Odd-numbered students should choose the gesture they are going to use as their "bow" at this time.

2. Each even-numbered student introduces their partner to the class and the introduced person "takes a bow" in whatever way they want. Example: Even-numbered student says "This is my new friend, Betty Crocker. She is here to learn techniques that might help her be a better cooking teacher." Then, Betty might pantomime stirring a pot.

3. All even-numbered students will introduce themselves to their partners.

4. Odd-numbered students, take turns introducing their partners to the room, giving each introduced person a moment to "take a bow".

Discussion: How do we learn?
----------------------------

Ask students how they learn best. Most technical types will say, "by doing". The goal is to get to this list: "Hearing, Seeing, Doing". Then, introduce the "nursing school method": See one, do one, teach one. Note that attentiveness and retention rise sharply in students that expect to have an opportunity to teach what they have been taught.
